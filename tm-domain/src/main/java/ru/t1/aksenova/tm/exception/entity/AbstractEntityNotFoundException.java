package ru.t1.aksenova.tm.exception.entity;

import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.exception.AbstractException;

public abstract class AbstractEntityNotFoundException extends AbstractException {

    public AbstractEntityNotFoundException() {
    }

    public AbstractEntityNotFoundException(@Nullable final String message) {
        super(message);
    }

    public AbstractEntityNotFoundException(
            @Nullable final String message,
            @Nullable final Throwable cause
    ) {
        super(message, cause);
    }

    public AbstractEntityNotFoundException(@Nullable final Throwable cause) {
        super(cause);
    }

    public AbstractEntityNotFoundException(
            @Nullable final String message,
            @Nullable final Throwable cause,
            boolean enableSuppression,
            boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
