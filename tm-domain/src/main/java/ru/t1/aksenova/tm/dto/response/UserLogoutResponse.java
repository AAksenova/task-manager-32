package ru.t1.aksenova.tm.dto.response;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class UserLogoutResponse extends AbstractResponse {

}
