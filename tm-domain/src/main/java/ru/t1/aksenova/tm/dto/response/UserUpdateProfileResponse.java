package ru.t1.aksenova.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.model.User;

@NoArgsConstructor
public final class UserUpdateProfileResponse extends AbstractUserResponse {

    public UserUpdateProfileResponse(@Nullable final User user) {
        super(user);
    }

}
