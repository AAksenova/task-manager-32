package ru.t1.aksenova.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.api.endpoint.ISystemEndpointClient;
import ru.t1.aksenova.tm.dto.request.ApplicationAboutRequest;
import ru.t1.aksenova.tm.dto.request.ApplicationVersionRequest;
import ru.t1.aksenova.tm.dto.response.ApplicationAboutResponse;
import ru.t1.aksenova.tm.dto.response.ApplicationVersionResponse;

public final class SystemEndpointClient extends AbstractEndpointClient implements ISystemEndpointClient {

    @NotNull
    @SneakyThrows
    public ApplicationAboutResponse getAbout(@NotNull final ApplicationAboutRequest request) {
        return call(request, ApplicationAboutResponse.class);
    }

    @NotNull
    @SneakyThrows
    public ApplicationVersionResponse getVersion(@NotNull ApplicationVersionRequest request) {
        return call(request, ApplicationVersionResponse.class);
    }

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        @NotNull final ApplicationAboutResponse serverAboutResponse = client.getAbout(new ApplicationAboutRequest());
        System.out.println("[About]");
        System.out.println("AUTHOR: " + serverAboutResponse.getName());
        System.out.println("E-MAIL: " + serverAboutResponse.getEmail());

        @NotNull final ApplicationVersionResponse serverVersionResponse = client.getVersion(new ApplicationVersionRequest());
        System.out.println("[Version]");
        System.out.println(serverVersionResponse.getVersion());

        client.disconnect();
    }

}
