package ru.t1.aksenova.tm.command.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.api.endpoint.IDomainEndpointClient;
import ru.t1.aksenova.tm.command.AbstractCommand;
import ru.t1.aksenova.tm.enumerated.Role;

public abstract class AbstractDataCommand extends AbstractCommand {

    @Nullable
    protected IDomainEndpointClient getDomainEndpointClient() {
        return getServiceLocator().getDomainEndpointClient();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
