package ru.t1.aksenova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.dto.request.UserLoginRequest;
import ru.t1.aksenova.tm.dto.request.UserLogoutRequest;
import ru.t1.aksenova.tm.dto.request.UserViewProfileRequest;
import ru.t1.aksenova.tm.dto.response.UserLoginResponse;
import ru.t1.aksenova.tm.dto.response.UserLogoutResponse;
import ru.t1.aksenova.tm.dto.response.UserViewProfileResponse;

public interface IAuthEndpointClient extends IEndpointClient {

    @NotNull
    UserLoginResponse login(@NotNull UserLoginRequest request);

    @NotNull
    UserLogoutResponse logout(@NotNull UserLogoutRequest request);

    @NotNull
    UserViewProfileResponse viewProfile(@NotNull UserViewProfileRequest request);

}
