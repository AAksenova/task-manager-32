package ru.t1.aksenova.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.request.DataBase64SaveRequest;
import ru.t1.aksenova.tm.enumerated.Role;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-save-base64";

    @NotNull
    public static final String DESCRIPTION = "Save data to base64 file.";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA SAVE BASE64]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest();
        getDomainEndpointClient().dataBase64Save(request);
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }
}
