package ru.t1.aksenova.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.aksenova.tm.dto.request.*;
import ru.t1.aksenova.tm.dto.response.*;

public interface IDomainEndpointClient extends IEndpointClient {

    @NotNull
    DataBackupLoadResponse dataBackupLoad(@NotNull DataBackupLoadRequest request);

    @NotNull
    DataBackupSaveResponse dataBackupSave(@NotNull DataBackupSaveRequest request);

    @NotNull
    DataBase64LoadResponse dataBase64Load(@NotNull DataBase64LoadRequest request);

    @NotNull
    DataBase64SaveResponse dataBase64Save(@NotNull DataBase64SaveRequest request);

    @NotNull
    DataBinaryLoadResponse dataBinaryLoad(@NotNull DataBinaryLoadRequest request);

    @NotNull
    DataBinarySaveResponse dataBinarySave(@NotNull DataBinarySaveRequest request);

    @NotNull
    DataJsonLoadFasterXmlResponse dataJsonLoadFasterXml(@NotNull DataJsonLoadFasterXmlRequest request);

    @NotNull
    DataJsonSaveFasterXmlResponse dataJsonSaveFasterXml(@NotNull DataJsonSaveFasterXmlRequest request);

    @NotNull
    DataJsonLoadJaxbResponse dataJsonLoadJaxb(@NotNull DataJsonLoadJaxbRequest request);

    @NotNull
    DataJsonSaveJaxbResponse dataJsonSaveJaxb(@NotNull DataJsonSaveJaxbRequest request);

    @NotNull
    DataXmlLoadFasterXmlResponse dataXmlLoadFasterXml(@NotNull DataXmlLoadFasterXmlRequest request);

    @NotNull
    DataXmlSaveFasterXmlResponse dataXmlSaveFasterXml(@NotNull DataXmlSaveFasterXmlRequest request);

    @NotNull
    DataXmlLoadJaxbResponse dataXmlLoadJaxb(@NotNull DataXmlLoadJaxbRequest request);

    @NotNull
    DataXmlSaveJaxbResponse dataXmlSaveJaxb(@NotNull DataXmlSaveJaxbRequest request);

    @NotNull
    DataYamlLoadFasterXmlResponse dataYamlLoadFasterXml(@NotNull DataYamlLoadFasterXmlRequest request);

    @NotNull
    DataYamlSaveFasterXmlResponse dataYamlSaveFasterXml(@NotNull DataYamlSaveFasterXmlRequest request);

}
