package ru.t1.aksenova.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aksenova.tm.dto.request.TaskShowByIndexRequest;
import ru.t1.aksenova.tm.dto.response.TaskShowByIndexResponse;
import ru.t1.aksenova.tm.model.Task;
import ru.t1.aksenova.tm.util.TerminalUtil;

public final class TaskShowByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-show-by-index";

    @NotNull
    public static final String DESCRIPTION = "Display task by index.";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public void execute() {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER TASK INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;

        @Nullable final TaskShowByIndexRequest request = new TaskShowByIndexRequest(index);
        @Nullable final TaskShowByIndexResponse response = getTaskEndpointClient().showTaskByIndex(request);
        @Nullable final Task task = response.getTask();
        showTask(task);
    }

}
